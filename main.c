#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <winsock2.h>
#include <Windows.h>

#define BUF_SIZE 30

void ErrorHandling(char *message);

int main(int argc, char *argv[])
{
	WSADATA wsaData;
	SOCKET sock;
	char message[BUF_SIZE];
	int strLen;
	int maxValue;
	int clientNum;

	SOCKADDR_IN servAdr;

	srand(time(NULL));

	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
		ErrorHandling("WSAStartup() error!");

	sock = socket(PF_INET, SOCK_DGRAM, 0);
	if (sock == INVALID_SOCKET)
		ErrorHandling("socket() error");

	memset(&servAdr, 0, sizeof(servAdr));
	servAdr.sin_family = AF_INET;
	servAdr.sin_addr.s_addr = inet_addr("127.0.0.1");
	servAdr.sin_port = htons(9000);

	connect(sock, (SOCKADDR*)&servAdr, sizeof(servAdr));

	*message = 1;
	send(sock, message, strlen(message), 0);

	strLen = recv(sock, message, sizeof(message) - 1, 0);
	printf("[클라이언트%d]\n", *message);
	clientNum = *message;

	recv(sock, message, sizeof(message)-1, 0);
	if (*message == 2) {
		printf("서버에서 지정한 범위 : 0 ~ %d\n", *((int *)(message + 1)));
		maxValue = *((int *)(message + 1));
	}

	recv(sock, message, sizeof(message)-1, 0);
	if (*message == 3) {
		printf("시작!!\n");
	}

	
	while (1) {
		int value = rand() % maxValue + 5;
		*message = 4;
		*(message + 1) = clientNum;
		*((int *)(message + 2)) = value;
		send(sock, message, sizeof(message), 0);
		printf("[신호 전송] : %d\n", value);
		Sleep(1000);

		recv(sock, message, sizeof(message)-1, 0);
		if (*message == 5) break;
	}

	system("pause");

	closesocket(sock);
	WSACleanup();

	return 0;
}

void ErrorHandling(char *message)
{
	fputs(message, stderr);
	fputc('\n', stderr);
	exit(1);
}
